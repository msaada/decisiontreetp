%1: Yes; 2: No
PlayTennis = [0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0]';
%1: Sunny; 2: Overcast; 3: Rain;
Outlook = [1, 1, 2, 3, 3, 3, 2, 1, 1, 3, 1, 2, 2, 3]';
%1: Hot; 2: Mild; 3: Cool
Temperature = [1, 1, 1, 2, 3, 3, 3, 2, 3, 2, 2, 2, 1, 2]';
%1: High; 2: Normal
Humidity = [1, 1, 1, 1, 2, 2, 2, 1, 2, 2, 2, 1, 2, 1]';
%1: Strong; 2: Weak
Wind = [2, 1, 2, 2, 2, 1, 1, 2, 2, 2, 1, 1, 2, 1]';


Attributes = [Outlook Temperature Humidity Wind PlayTennis];

